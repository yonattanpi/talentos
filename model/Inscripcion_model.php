<?php

class Inscripcion_model extends Sqlserver {

  function __construct() {
    
  }

  function listarVideos() {

    $sql = " SELECT Consecutivo
      ,NombreEmpleado
      ,Link
      ,Area
      ,FechaCreacion
      ,UsuarioCreacion
      FROM Paso.dbo.TalentoParticipantes ORDER BY NEWID()";

    //echo $sql;

    $this->consultar($sql);
  }

  function listarResultados() {

    $sql = " SELECT     
      tv.Voto
     ,count(tv.Voto) as Cantidad  
     ,tp.NombreEmpleado
     ,tp.Area          
    FROM Paso.dbo.TalentoVoto tv INNER JOIN Paso.dbo.TalentoParticipantes tp
    ON tv.Voto=tp.Consecutivo  GROUP BY tv.Voto, tp.NombreEmpleado, tp.Area Order by 2 DESC";

    //echo $sql;

    $this->consultar($sql);
  }

}
