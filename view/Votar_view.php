<?php

class Votar_view extends VistaBase {

  function __construct() {
    
  }

  function index() {
    ?>
    <script src="resources/js/Votar.js"></script>

    <div class="content-wrapper" style="min-height: 100%">
      <section class="content">
        <div class="row">
          <div class="col-md-10 col-md-offset-1">

            <div class="box box-default">
              <div class="box-header with-border">
                <h3 class="box-title">Votar</h3>
                <!--<div class="box-tools pull-right">
                  <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                  </button>
                  <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                </div>-->
              </div>
              <!-- /.box-header -->
              <div class="box-body">

                <div class="row">
                  <div id="div-cargando"  class="col-md-2 col-md-offset-5" style="margin-bottom: 15px">
                  </div>
                </div>
                <div class="alert bg-danger alert-dismissible " role="alert" id="alert-crud" style="display: none">
                    <!--<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>-->
                  <span id="span-mensaje"><strong>Warning!</strong> Better check yourself, you're not looking too good.</span>
                </div>
                <div class="row">
                  <div class="col-md-10 col-md-offset-1 text-center" id="div-PlaneacionEncuesta">
                    <?php
                    $formulario = $this->pintarFormulario('crear', 'Aceptar');
                    echo $formulario;
                    ?>
                  </div>
                </div>
                <!-- /.row -->

              </div>
              <!-- /.box-body -->
            </div>

          </div>
        </div>

      </section>
    </div>
    <?php
  }

  function pintarFormulario($metodo, $labelBtn) {
    ob_start();
    ?>

    <table width="100%" border="0" cellpadding="2">
      <?php
      //$i = $this->datos->getRegistros();
      $i = 0;
      while ($registro = $this->datos->getRegistro()) {

        if ($i == 0) {
          ?>

          <tr>

            <?php
          }

          if ($i % 2 == 0) {
            ?>

          </tr>
          <tr>
            <td>          

              <div align="center"> 
                <b><?php echo "Funcionario: " . ucfirst(strtolower($registro['NombreEmpleado'])); ?></b></br> 
                <b><?php echo "Área: " . ucfirst(strtolower($registro['Area'])); ?></b> 
              </div>

              <iframe width="380" height="200" src="<?php echo $registro['Link']; ?>" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

              <div align="center"> 
                <button  class="btn btn-primary" id="btn-Inscripcion"  onclick="votar(<?php echo $registro['Consecutivo']; ?>)">Votar</button>
              </div>

            </td>
            <?php
          } else {
            ?>

            <td>

              <div align="center"> 
                <b><?php echo "Funcionario: " . ucfirst(strtolower($registro['NombreEmpleado'])); ?></b></br> 
                <b><?php echo "Área: " . ucfirst(strtolower($registro['Area'])); ?></b> 
              </div> 

              <iframe width="380" height="200" src="<?php echo $registro['Link']; ?>" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
             
              <div align="center"> 
                <button  class="btn btn-primary" id="btn-Inscripcion" style="margin-left: 15px; margin-bottom: 15px"  onclick="votar(<?php echo $registro['Consecutivo']; ?>)">Votar</button>   
              </div>
              
            </td>

            <?php
          }
          $i++;
        }
        ?>				

    </table>

    <?php
    $resultado = ob_get_contents();
    ob_end_clean();
    return $resultado;
  }

}
