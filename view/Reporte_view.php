<?php
	class Reporte_view extends VistaBase {
					function __construct() {}

					function index(){?>
					
	
				<div class="content-wrapper" style="min-height: 100%">
				<section class="content">
					<div class="row">
						<div class="col-md-10 col-md-offset-1">
              
							<div class="box box-default">
								<div class="box-header with-border">
									<h3 class="box-title">Resultados</h3>
									<!--<div class="box-tools pull-right">
										<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
										</button>
										<button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
									</div>-->
								</div>
								<!-- /.box-header -->
								<div class="box-body">
									
                  <div class="row">
										<div id="div-cargando"  class="col-md-2 col-md-offset-5" style="margin-bottom: 15px">
										</div>
									</div>
									<div class="alert bg-danger alert-dismissible " role="alert" id="alert-crud" style="display: none">
											<!--<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>-->
											<span id="span-mensaje"><strong>Warning!</strong> Better check yourself, you're not looking too good.</span>
									</div>
									<div class="row">
										<div class="col-md-10 col-md-offset-1 text-center" id="div-Resultados">
											<?php $formulario = $this->pintarTabla('tablaResultado'); 
											echo $formulario;					
											?>
										</div>
									</div>
									<!-- /.row -->
                  
								</div>
								<!-- /.box-body -->
							</div>
              
						</div>
					</div>
                    
				</section>
			</div>
		<?php 
		}
		
	 function pintarTabla($idTabla) {
    ob_start();
    ?>

    <table class="table table-striped tablas table-bordered"  id="tabla-<?php echo $idTabla ?>" >
      <thead>      	     
      <th>Nombre</th>	
      <th>Área</th>	
      <th>Votos</th>			       
    </thead>
    <tbody>																		<?php
     
      while ($registro = $this->datos->getRegistro()) {     
        ?>
        <tr id="tr-<?php echo $registro[Voto] ?>">
          <td><?php echo ucfirst(strtolower($registro['NombreEmpleado'])); ?></td>
          <td><?php echo ucfirst(strtolower($registro['Area'])); ?></td>        
          <td><?php echo $registro['Cantidad']; ?></td>                           
        </tr>
        <?php
      }
      ?>				
    </tbody>
    </table>

    <?php
    $resultado = ob_get_contents();
    ob_end_clean();
    return $resultado;
  }
		
	
	}
