</div>

<style>
  .modal-lg{
  width: 100%;
}

.table td {
   border: 1px solid #444 !important;   
}

.table th {
   border: 1px solid #444 !important; 
}


</style>

<div class="modal fade" id="ModalGeneral" tabindex="-1" role="dialog" aria-labelledby="ModalLabel" >
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="ModalGeneralLabel"></h4>
      </div>

      <div class="modal-body" id="ModalGeneralMessage">
        <!--pestañas-->
        <div class="nav-tabs-custom">
          <ul class="nav nav-tabs">
            <li class="active"><a href="#tab_1" data-toggle="tab">Resumen Mes</a></li>
            <li><a href="#tab_2" data-toggle="tab">Resumen Semana</a></li>
            <li><a href="#tab_3" data-toggle="tab">Detallado x Hora</a></li>
            <li><a href="#tab_4" data-toggle="tab">Resumen Calendario</a></li>
          </ul>
          <div class="tab-content">
            <div class="tab-pane active table-responsive" id="tab_1">
            </div>
            <!-- /.tab-pane -->
            <div class="tab-pane table-responsive" id="tab_2">
            </div>
            <!-- /.tab-pane -->
            <div class="tab-pane table-responsive" id="tab_3">
            </div>
            <!-- /.tab-pane -->
            <div class="tab-pane table-responsive" id="tab_4">
            </div>
            <!-- /.tab-pane -->
          </div>
          <!-- /.tab-content -->
        </div>
      </div>
      
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
      </div>

    </div>
  </div>
</div>

<!-- SlimScroll -->
<script src="<?php echo APPLICATION ?>resources/lib/slimScroll/jquery.slimscroll.min.js" type="text/javascript"></script>

<!-- AdminLTE for demo purposes -->
<script src="<?php echo APPLICATION ?>resources/css/dist/js/demo.js" type="text/javascript"></script>    

<!-- Bootstrap 3.3.2 JS -->
<script src="<?php echo APPLICATION ?>resources/lib/bootstrap/js/bootstrap.js" type="text/javascript"></script>


<!-- AdminLTE App -->
<script src="<?php echo APPLICATION ?>resources/css/dist/js/app.min.js" type="text/javascript"></script>

<!-- DATA TABES SCRIPT -->
<script src="<?php echo APPLICATION ?>resources/lib/datatables/jquery.dataTables.js" type="text/javascript"></script>
<script src="<?php echo APPLICATION ?>resources/lib/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
<!--botones datatable -->	 

<link href="<?php echo APPLICATION ?>resources/lib/select/css/bootstrap-select.css" rel="stylesheet" type="text/css" />  
<script src="<?php echo APPLICATION ?>resources/lib/select/js/bootstrap-select.js" type="text/javascript"></script>

<link rel="stylesheet" href="<?php echo APPLICATION ?>resources/lib/jquery-confirm/dist/jquery-confirm.min.css">
<script src="<?php echo APPLICATION ?>resources/lib/jquery-confirm/js/jquery-confirm.js"></script>



</body>
</html>