<?php
//var_dump($this->menuPermisos);
?>
<style>

  .dropdown-submenu {
    position:relative;
  }
  .dropdown-submenu>.dropdown-menu {
    top:0;
    left:100%;
    margin-top:-6px;
    margin-left:-1px;
    -webkit-border-radius:0 6px 6px 6px;
    -moz-border-radius:0 6px 6px 6px;
    border-radius:0 6px 6px 6px;
    background-color: #dddddd;
  }
  .dropdown-submenu:hover>.dropdown-menu {
    display:block;
  }
  .dropdown-submenu>a:after {
    display:block;
    content:" ";
    float:right;
    width:0;
    height:0;
    border-color:transparent;
    border-style:solid;
    border-width:5px 0 5px 5px;
    border-left-color:#cccccc;
    margin-top:5px;
    margin-right:-10px;
  }
  .dropdown-submenu:hover>a:after {
    border-left-color:#ffffff;
  }
  .dropdown-submenu.pull-left {
    float:none;
  }
  .dropdown-submenu.pull-left>.dropdown-menu {
    left:-100%;
    margin-left:10px;
    -webkit-border-radius:6px 0 6px 6px;
    -moz-border-radius:6px 0 6px 6px;
    border-radius:6px 0 6px 6px;
  }	

  /*Se comenta porque se cruza con el estilo del selected multiple*/
  /*	body{
      background: #8999A8;
    }
    .navbar, .dropdown-menu{
      background:rgba(255,255,255,0.95);
      border: none;
  
    }
  
    .nav>li>a, .dropdown-menu>li>a:focus, .dropdown-menu>li>a:hover, .dropdown-menu>li>a, .dropdown-menu>li{
      border-bottom: 3px solid transparent;
    }
    .nav>li>a:focus, .nav>li>a:hover,.nav .open>a, .nav .open>a:focus, .nav .open>a:hover, .dropdown-menu>li>a:focus, .dropdown-menu>li>a:hover{
      border-bottom: 3px solid transparent;
      background: none;
    }
    .navbar a, .dropdown-menu>li>a, .dropdown-menu>li>a:focus, .dropdown-menu>li>a:hover, .navbar-toggle{
      color: #fff;
    }
    .dropdown-menu{
      -webkit-box-shadow: none;
      box-shadow:none;
    }*/

  .nav li:hover:nth-child(8n+1), .nav li.active:nth-child(8n+1){
    border-bottom: #C4E17F 3px solid;
  }
  .nav li:hover:nth-child(8n+2), .nav li.active:nth-child(8n+2){
    border-bottom: #F7FDCA 3px solid;
  }
  .nav li:hover:nth-child(8n+3), .nav li.active:nth-child(8n+3){
    border-bottom: #FECF71 3px solid;
  }
  .nav li:hover:nth-child(8n+4), .nav li.active:nth-child(8n+4){
    border-bottom: #F0776C 3px solid;
  }
  .nav li:hover:nth-child(8n+5), .nav li.active:nth-child(8n+5){
    border-bottom: #DB9DBE 3px solid;
  }
  .nav li:hover:nth-child(8n+6), .nav li.active:nth-child(8n+6){
    border-bottom: #C49CDE 3px solid;
  }
  .nav li:hover:nth-child(8n+7), .nav li.active:nth-child(8n+7){
    border-bottom: #669AE1 3px solid;
  }
  .nav li:hover:nth-child(8n+8), .nav li.active:nth-child(8n+8){
    border-bottom: #62C2E4 3px solid;
  }

  .navbar-toggle .icon-bar{
    color: #fff;
    background: #fff;
  }	

</style>
<header class="main-header">               
  <nav class="navbar navbar-static-top color-header"  style="border-bottom: 10px;">
    <div class="container">

      <!-- Brand and toggle get grouped for better mobile display -->
      <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
          <span class="sr-only"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
      </div>

      <!-- Collect the nav links, forms, and other content for toggling -->
      <div class="collapse navbar-collapse "  id="bs-example-navbar-collapse-1">
        <ul class="nav navbar-nav pull-left " style="margin-right: 15px">

           <li class="dropdown ">
            <a href="#" class="dropdown-toggle text-color-black" data-toggle="dropdown"><i class="fa fa-eye" style="margin-right: 10px"></i>Inicio<span class="caret"></span></a>
            <ul class="dropdown-menu" role="menu">    

              <li><a href="index.php" class="text-color-black2">Página Inicio</a></li> 
              <li><a href="index.php?controlador=Reporte" class="text-color-black2">Reporte</a></li>

            </ul>
          </li> 
          
        
        </ul>
      </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
  </nav>
</header>

