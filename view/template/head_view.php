<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title>Comfenalco</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <!-- Bootstrap 3.3.4 -->
    <link href="<?php echo APPLICATION ?>resources/lib/bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css" />
    <!-- Font Awesome Icons -->
    <link href="<?php echo APPLICATION ?>resources/lib/font-awesome-4.3.0/css/font-awesome.css" rel="stylesheet" type="text/css" />
    
    <!-- Theme style -->
    <link href="<?php echo APPLICATION ?>resources/css/dist/css/AdminLTE.css" rel="stylesheet" type="text/css" />
    <!-- AdminLTE Skins. Choose a skin from the css/skins 
         folder instead of downloading all of them to reduce the load. -->
    <link href="<?php echo APPLICATION ?>resources/css/dist/css/skins/_all-skins.css" rel="stylesheet" type="text/css" />
    
    <link href="<?php echo APPLICATION ?>resources/lib/amaran/css/amaran.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo APPLICATION ?>resources/lib/amaran/css/animate.min.css" rel="stylesheet" type="text/css" />    

    <!--<link href="resources/css/empleado.css"  rel="stylesheet" type="text/css" /> -->
    <!-- jQuery 2.1.4 -->
   <script src="<?php echo APPLICATION ?>resources/lib/jQuery/jQuery-2.1.4.min.js"></script>   
  
    <!-- DATA TABLES -->
    <link href="<?php echo APPLICATION ?>resources/lib/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo APPLICATION ?>resources/lib/datatables/buttons.dataTables.min.css" rel="stylesheet" type="text/css" />  
    <link href="<?php echo APPLICATION ?>resources/lib/datatables/buttons.dataTables.min.css" rel="stylesheet" type="text/css" />  
    <link href="<?php echo APPLICATION ?>resources/lib/datatables/dataTables.fixedColumns.dataTables.min.css" rel="stylesheet" type="text/css" />    
    		
		
		<!--FORM VALIDATOR-->
		<link rel="stylesheet" href="<?php echo APPLICATION ?>resources/lib/formValidation/dist/css/formValidation.css"/>
		<script type="text/javascript" src="<?php echo APPLICATION ?>resources/lib/formValidation/dist/js/formValidation.js"></script>
		<script type="text/javascript" src="<?php echo APPLICATION ?>resources/lib/formValidation/dist/js/framework/bootstrap.js"></script>
		<script type="text/javascript" src="<?php echo APPLICATION ?>resources/lib/bootstrap/js/bootstrap-typeahead.min.js"></script>
		
    <!--date picket-->
		<script src="<?php echo APPLICATION ?>resources/lib/datepicker/bootstrap-datepicker.js" type="text/javascript"></script>
		
    <link href="<?php echo APPLICATION ?>resources/lib/datepicker/datepicker3.css"  rel="stylesheet" type="text/css" />
		<script src="<?php echo APPLICATION ?>resources/lib/datepicker/bootstrap-datepicker.js" type="text/javascript"></script>
    

    <script src="<?php echo APPLICATION ?>resources/lib/amcharts/amcharts/amcharts.js"></script>   
    <script src="<?php echo APPLICATION ?>resources/lib/amcharts/amcharts/serial.js"></script>   
    <script src="<?php echo APPLICATION ?>resources/lib/amcharts/amcharts/pie.js"></script>  
  </head>
  <body class="skin-blue layout-top-nav">
<div class="wrapper">