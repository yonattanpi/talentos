<?php
	class Inscripcion_view extends VistaBase {
					function __construct() {}

					function index(){?>
						<script src="resources/js/Inscripcion.js"></script>
	
				<div class="content-wrapper" style="min-height: 100%">
				<section class="content">
					<div class="row">
						<div class="col-md-10 col-md-offset-1">
              
							<div class="box box-default">
								<div class="box-header with-border">
									<h3 class="box-title">Inscripción</h3>
									<!--<div class="box-tools pull-right">
										<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
										</button>
										<button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
									</div>-->
								</div>
								<!-- /.box-header -->
								<div class="box-body">
									
                  <div class="row">
										<div id="div-cargando"  class="col-md-2 col-md-offset-5" style="margin-bottom: 15px">
										</div>
									</div>
									<div class="alert bg-danger alert-dismissible " role="alert" id="alert-crud" style="display: none">
											<!--<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>-->
											<span id="span-mensaje"><strong>Warning!</strong> Better check yourself, you're not looking too good.</span>
									</div>
									<div class="row">
										<div class="col-md-10 col-md-offset-1 text-center" id="div-PlaneacionEncuesta">
											<?php $formulario = $this->pintarFormulario('crear','Aceptar'); 
											echo $formulario;					
											?>
										</div>
									</div>
									<!-- /.row -->
                  
								</div>
								<!-- /.box-body -->
							</div>
              
						</div>
					</div>
                    
				</section>
			</div>
		<?php 
		}
		
		function pintarFormulario($metodo,$labelBtn){
		ob_start();
		?>
		
 <form class="form-horizontal" id="form-Inscripcion" >	
		
  <div class="form-group">
		
    <!--<label style="color: #006b3e;"><h4><b>Información del cliente</b></h4></label>-->
 
	</div>		  
    
   <label class="control-label col-sm-12" for="nombre"></label>
   
	<div class="form-group">
		<label class="control-label col-sm-2" for="nombre">Nombre Funcionario:</label>
		<div class="col-sm-10">
											<input type="text" class="form-control " id="nombre" name="nombre"  placeholder="">							
		</div>
	</div>		
						
	<div class="form-group">
		<label class="control-label col-sm-2" for="area">Área:</label>
		<div class="col-sm-10">
											<input type="text" class="form-control " id="area" name="area"  placeholder="">							
		</div>
	</div>		
   
   <div class="form-group">
		<label class="control-label col-sm-2" for="area">Link:</label>
		<div class="col-sm-10">
					<input type="text" class="form-control " id="link" name="link"  placeholder="">							
		</div>
	</div>		

	<div class="row">
		<div class="col-md-1 col-md-offset-5">
			<button type="submit" class="btn btn-primary" id="btn-Inscripcion" style="margin-left: 15px;margin-bottom: 15px"><?php echo $labelBtn?></button>
			<input name="controlador" id="controlador" type="hidden" value="Inscripcion">
			<input name="metodo" id="metodo" type="hidden" value="<?php echo $metodo?>">
		</div>
	</div>			

	</form>
	
            
  <div class="row">
		<div class="col-md-1 col-md-offset-5">
			</br>
      </br>
      <button  class="btn btn-primary" id="btn-Inscripcion" style="margin-left: 15px;margin-bottom: 15px"  onclick="votar()">Votar</button>      
		</div>
	</div>	        
                                    
            
		<?php
		$resultado = ob_get_contents();
		ob_end_clean();
		return $resultado;
		}
		
	
	}
