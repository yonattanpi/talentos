$(document).ready(function () {



});

function votar(id) {

  $.ajax({
    url: 'index.php',
    data: {"metodo": "crear", "controlador": "Votar", "id": id},
    type: 'POST',
    dataType: 'json',
    beforeSend: function (xhr, settings) {
      //$("#" + v_cargando).html("<img src='resources/img/cargando.gif' />");
      /*sus acciones*/
    },
    success: function (data) {
      //$("#" + v_contenedor).html(data.tabla);
      //$("#" + v_cargando).html("");

      if (data.exito == "1") {

        $.alert({
          title: 'Mensaje',
          content: 'Su voto ha sido registrado',
          buttons: {
            confirm: {
              text: 'Aceptar',
              btnClass: 'btn-blue',
              action: function () {

              }
            }
          }
        });

      } else {

        $.alert({
          title: 'Error',
          content: 'Intente de nuevo',
          buttons: {
            confirm: {
              text: 'Aceptar',
              btnClass: 'btn-blue',
              action: function () {

              }
            }
          }
        });

      }

    },
    complete: function (xhr, status) {


    }
  });

}
