$(document).ready(function () {
	validarEnviarForm();  
  
 
});

function validarEnviarForm() {
  
  
	$("#form-Inscripcion").formValidation({
		fields: {		           
    
      "nombre": {
        row: '.col-sm-10',
        validators: {          
          notEmpty: {
            message: 'Campo requerido'
          }, 
          stringLength: {
            min: 3,            
            message: 'Completar el campo'
          },       
          
        }
      },
      "area": {
        row: '.col-sm-10',
        validators: {          
          notEmpty: {
            message: 'Campo requerido'
          }, 
          stringLength: {
            min: 3,            
            message: 'Completar el campo'
          },       
          
        }
      },
      
      "link": {
        row: '.col-sm-10',
        validators: {          
          notEmpty: {
            message: 'Campo requerido'
          }, 
          stringLength: {
            min: 3,            
            message: 'Completar el campo'
          },       
          
        }
      }
           
                  
		}
	}).on('success.form.fv', function (e) {
    
		e.preventDefault();
		if ($("#form-Inscripcion").data('formValidation').isValid() === false) {
			return false;
		}

		var form = $("#form-Inscripcion").serialize();
		$("#alert-crud").hide();
		v_contenedor = "div-cargando";
		$.ajax({
			url: 'index.php',
			data: form,
			type: 'POST',
			dataType: 'json',
			beforeSend: function (xhr, settings) {
				$("#" + v_contenedor).html("<img src='resources/img/cargando.gif' />");
				
			},
			success: function (data) {
				//$( "#alert-conexion" ).show();
				if (data.exito == "1") {                  
          
					$("#alert-crud").removeClass().addClass("alert bg-success alert-dismissible");				
					$("#form-Inscripcion")[0].reset();
					$("#alert-crud").hide(1000);
          $('#btn-Inscripcion').attr("disabled", false);
					$('#btn-Inscripcion').removeClass("disabled");
          
				} else {                  
          
					$("#alert-crud").removeClass().addClass("alert bg-danger alert-dismissible");
					$('#btn-Inscripcion').attr("disabled", true);
					$('#btn-Inscripcion').removeClass("disabled");
				}
				$("#" + v_contenedor).html("");
				$("#span-mensaje").text(data.mensaje);
				$("#alert-crud").show();
			},
			error: function (xhr, status) {
			
			},
			complete: function (xhr, status) {
				
			}
		});
		return false;

	});
}

function votar() {
 
  window.location.href = "/comfenalcoNomina/TalentoShow/index.php?controlador=Votar";
  
}
