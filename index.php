<?php

/* Configuración global */
require_once 'config/global.php';

/* Funciones para el controlador frontal */
require_once RUTA_MVC . 'ControladorBase.php';
require_once RUTA_MVC . 'VistaBase.php';
require_once APPLICATION . 'helper/fechas.php';
require_once APPLICATION . 'helper/File.php';

/* Cargamos controladores y acciones */
if (isset($_REQUEST["controlador"]) && isset($_SESSION['session_comf_user']) ) {
  $controllerObj = cargarControlador($_REQUEST["controlador"]);
  lanzarAccion($controllerObj);
} else {
  $controllerObj = cargarControlador(CONTROLADOR_DEFECTO);
  lanzarAccion($controllerObj);
}
?>
