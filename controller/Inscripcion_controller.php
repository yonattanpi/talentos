<?php

class Inscripcion_controller extends ControladorBase {

  public $model;

  function __construct() {
    parent::__construct();
    $this->model = cargarModel('Inscripcion');
    $this->model->sqlserver(SERVER_NAME, USER, PASS);
    $this->menuPermisos = validarPermisosGlobales($this->model);
  }

  public function index() {
    $vista = cargarView('Inscripcion');
    $vista_datos = array();

    $vista_datos['menuPermisos'] = $this->menuPermisos;
    $vista->asignarVariable($vista_datos);

    $vista->cargarTemplate("head");
    $vista->cargarTemplate("menu");
    $vista->index();
    $vista->cargarTemplate("foot");
  }

  public function crear() {

    $this->model->transaction();
    $this->model->tabla = "Paso.dbo.TalentoParticipantes";
    $this->model->funcion = __FUNCTION__;

    $link = $_REQUEST['link'];
    $linkYoutube="";
    
    //dependiendo el enlace se configura 
    if (strpos($link, 'watch') !== false) {
       
      $id = explode("=", $link);  
      $linkYoutube="https://www.youtube.com/embed/".$id[1]."?rel=0";
     
    } else {
      
      $id = explode("/", $link); 
      $linkYoutube="https://www.youtube.com/embed/".$id[3]."?rel=0";
      
    }
    
    $insert = array();

    $insert['NombreEmpleado'] = "'" . $_REQUEST['nombre'] . "'";
    $insert['Area'] = "'" . $_REQUEST['area'] . "'";
    $insert['Link'] = "'" . $linkYoutube. "'";
    $insert['UsuarioCreacion'] = "'" . $_SESSION['session_comf_cedula'] . "'";
    $insert['FechaCreacion'] = "getdate()";

    $this->model->datos = $insert;
    $error = 0;
    $error += $this->model->insertar(false, true);
    //$error=0;
    if ($error > 0) {
      $this->model->rollback();
      echo json_encode(array("exito" => "0", "mensaje" => $this->model->mensajeLimpio));
    } else {
      $this->model->commit();

      echo json_encode(array("exito" => "1", "mensaje" => "Registro creado correctamente"));
    }
    
    
    
  }

}
