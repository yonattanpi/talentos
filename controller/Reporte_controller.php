<?php

class Reporte_controller extends ControladorBase {

  public $model;

  function __construct() {
    parent::__construct();
    $this->model = cargarModel('Inscripcion');
    $this->model->sqlserver(SERVER_NAME, USER, PASS);
    $this->menuPermisos = validarPermisosGlobales($this->model);
  }

  public function index() {
    $vista = cargarView('Reporte');
    $vista_datos = array();
   
    $vista_datos['menuPermisos'] = $this->menuPermisos;    
		    
    $this->model->listarResultados();     
    $datos = $this->model->getdatosBd();                   
    
    $vista_datos['datos'] = $datos;        
    $vista->asignarVariable($vista_datos);
    
    $vista->cargarTemplate("head");
    $vista->cargarTemplate("menu");
    $vista->index();
    $vista->cargarTemplate("foot");
  }

 
  
 



}
