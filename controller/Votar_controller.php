<?php

class Votar_controller extends ControladorBase {

  public $model;

  function __construct() {
    parent::__construct();
    $this->model = cargarModel('Inscripcion');
    $this->model->sqlserver(SERVER_NAME, USER, PASS);
    $this->menuPermisos = validarPermisosGlobales($this->model);
  }

  public function index() {
    $vista = cargarView('Votar');
    $vista_datos = array();
   
    $vista_datos['menuPermisos'] = $this->menuPermisos;    
		    
    $this->model->listarVideos();     
    $datos = $this->model->getdatosBd();                   
    
    $vista_datos['datos'] = $datos;        
    $vista->asignarVariable($vista_datos);
    
    $vista->cargarTemplate("head");
    $vista->cargarTemplate("menu");
    $vista->index();
    $vista->cargarTemplate("foot");
  }

  public function crear() {

    $this->model->transaction();
    $this->model->tabla = "Paso.dbo.TalentoVoto";
    $this->model->funcion = __FUNCTION__;  
       
    $insert = array();
  
    $insert['Voto'] = "'" . $_REQUEST['id'] . "'";   
    $insert['Usuario'] = "'" .$_SESSION['session_comf_cedula'] . "'";   
    $insert['Fecha'] = "getdate()";
   
    $this->model->datos = $insert;
    $error = 0;
    $error += $this->model->insertar(false, true);
    //$error=0;
    if ($error > 0) {
      $this->model->rollback();
      echo json_encode(array("exito" => "0", "mensaje" => $this->model->mensajeLimpio));
    } else {
      $this->model->commit();

      echo json_encode(array("exito" => "1", "mensaje" => "Registro creado correctamente"));
    }       
    
  }
    
    public function listar() {

    $vista = cargarView('Participante');

    $this->model->listarRegistros('');
    $datos = $this->model->getdatosBd();

    $vista_datos = array();
    $vista_datos['datos'] = $datos;

    $vista->asignarVariable($vista_datos);

    $resultado = $vista->pintarTabla('Participante');

    echo json_encode(array("tabla" => $resultado, "exito" => "1"));
    
  }
  
 



}
